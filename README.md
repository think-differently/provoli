# Provoli
Provoli is a modern wrapper for Tkinter. The name comes from the Greek word προβολή, which means 'to show'.

## Required modules
Required modules (install if necessary with `python -m pip install ___ --user`) are:
* tkinter (comes with python3)
* Pillow (comes with python3)
* pypiwin32
* win32gui

## Installing Dependencies
While the dependencies are mentioned in the [Required modules](#Required modules) section above, it is recommended to use the following command to be sure you have all the needed dependencies:
```
pip install Pillow pypiwin32 win32gui
```

## Cloning
Use the following commands to properly and fully clone this project:
```
git clone https://gitlab.com/think-differently/provoli.git
```

To update the project, use the following command:
```
git pull
```

To push any changes to the branch you are on, use the following commands:
```
git add .
git commit -m "A summary of the changes made"
git push
```