import sys
import ctypes
Xlib = ctypes.CDLL("libX11.so.6")
display = Xlib.XOpenDisplay(None)
if display == 0: sys.exit(2)
w = Xlib.XRootWindow(display, ctypes.c_int(0))
(root_id, child_id) = (ctypes.c_uint32(), ctypes.c_uint32())
(root_x, root_y, win_x, win_y) = (ctypes.c_int(), ctypes.c_int(), ctypes.c_int(), ctypes.c_int())
mask = ctypes.c_uint()
ret = Xlib.XQueryPointer(display, ctypes.c_uint32(w), ctypes.byref(root_id), ctypes.byref(child_id),
                         ctypes.byref(root_x), ctypes.byref(root_y),
                         ctypes.byref(win_x), ctypes.byref(win_y), ctypes.byref(mask))
if ret == 0: sys.exit(1)
print(child_id.value)
print(root_x.value, root_y.value, win_x.value, win_y.value)