import tkinter as tk
import tkinter.ttk as ttk
import win32api  # pip install pypiwin32
import win32gui  # pip install win32gui
from ctypes import windll
GWL_EXSTYLE = -20
WS_EX_APPWINDOW = 0x00040000
WS_EX_TOOLWINDOW = 0x00000080
_support_default_root = 1
_default_root = None

class Win():
    def __init__(self, root):
        self.root = root
        self.root.wm_title("Minimal Test")
        self.monitor_info = win32api.GetMonitorInfo(
            win32api.MonitorFromPoint((0, 0)))
        self.work_area = self.monitor_info.get("Work")
        self.root.geometry("{}x{}+0+0".format(self.work_area[2], self.work_area[3]))

        self.state = 'zoomed'
        self.root.state(self.state)  # Windows and macOS zoomed
        self.root.update_idletasks()
        self.root.overrideredirect(True)
        self.root.after(1, lambda: self.set_appwindow())
        self.root.lift()

        self.i_height = 500
        self.i_width = 500
        self._offsetx = 0
        self._offsety = 0
        self.x = 0
        self.y = 0

        self.root_frame = tk.Frame(self.root, height=self.work_area[3], width=self.root.winfo_width(), bg='white', bd=1)
        self.root_frame.pack(fill=tk.BOTH, expand=tk.YES)

        self.grip1 = ttk.Sizegrip(self.root)
        self.grip2 = ttk.Sizegrip(self.root)
        self.grip3 = ttk.Sizegrip(self.root)
        self.grip4 = ttk.Sizegrip(self.root)

        self.top = tk.Frame(self.root_frame, width=self.root.winfo_width(), height=30, bg='lightgray')
        self.top.pack(side=tk.TOP, fill=tk.X)

        self.closeBtn = tk.Button(self.top, text="Close", command=lambda: self.root.destroy(), width=10, height=25, padx=5, highlightthickness=0, bd=0, activebackground="red", bg='lightgray', disabledforeground="darkgray")
        self.closeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=2)

        self.zoomBtn = tk.Button(self.top, text="Zoom", command=lambda: self.zoom(x=self.x, y=self.x), width=10, height=25, padx=5, highlightthickness=0, bd=0, activebackground="gray", bg='lightgray', disabledforeground="darkgray")
        self.zoomBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=2)

        self.set_bindings()
        self.update()

    def set_appwindow(self):
        hwnd = windll.user32.GetParent(self.root.winfo_id())
        style = windll.user32.GetWindowLongW(hwnd, GWL_EXSTYLE)
        style = style | WS_EX_APPWINDOW
        self.res = windll.user32.SetWindowLongW(hwnd, GWL_EXSTYLE, style)
        self.root.wm_withdraw()
        self.root.after(10, lambda: self.root.wm_deiconify())
        self.root.focus_force()
        self.root.protocol('WM_DELETE_WINDOW', lambda: self.root.destroy())
        self.root.update_idletasks()

    def set_bindings(self):
        self.top.bind(
            "<ButtonPress-1>", self.StartMove)
        self.top.bind(
            "<B1-Motion>", self.OnMotion)

        self.root_frame.bind('<B1-Motion>', self.resize)
        self.grip1.bind("<B1-Motion>", lambda event: self.resize(event,'se'))
        self.grip2.bind("<B1-Motion>", lambda event: self.resize(event, 'sw'))
        self.grip3.bind("<B1-Motion>", lambda event: self.resize(event, 'ne'))
        self.grip4.bind("<B1-Motion>", lambda event: self.resize(event, 'nw'))

    def update(self):
        self.root.update_idletasks()

        self.top.place(x=0, y=0, width=self.root.winfo_width(), height=30)
        self.closeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=2)
        self.zoomBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=2)

        self.root.update_idletasks()

    def geometry(self, geo):
        self.root.geometry(geo)
        if self.state == 'unzoomed':
            if 'x' in geo:
                self.i_width = geo[0:geo.find('x')]
                if '+' in geo:
                    self.i_height = geo[geo.find('x'):geo.find('+')]
                else:
                    self.i_height = geo[geo.find('x')+1:len(geo)]

    def preresize(self):
        self.cx = self.root.winfo_pointerx()
        self.cy = self.root.winfo_pointery()

    def resize(self, event, pos=None):
        x1 = self.root.winfo_pointerx()
        y1 = self.root.winfo_pointery()
        x0 = self.root.winfo_rootx()
        y0 = self.root.winfo_rooty()
        if pos=='sw':
            self.x = x1-event.x_root
            self.root.geometry("{0}x{1}+{2}+{3}".format((x1+x0), (y1-y0), self.x, self.y)) # x, w, h
        elif pos=='se':
            self.root.geometry("{0}x{1}".format((x1-x0), (y1-y0))) # w, h # DONE
        elif pos=='nw':
            self.x = x1-event.x_root
            self.y = y1-event.y_root
            if x1-x0 < 0:
                self.root.geometry("{0}x{1}+{2}+{3}".format((x1+x0), (y1-y0+self.i_width), x1, y1)) # x, y, w, h
            else:
                self.root.geometry("{0}x{1}+{2}+{3}".format((x1-x0), (y1+y0), x1, y1)) # x, y, w, h
        elif pos=='ne':
            self.y = y1-event.y_root
            self.root.geometry("{0}x{1}+{2}+{3}".format((x1-x0), (y0+y1), self.x, self.y)) # y, w, h
        else:
            self.update()
        self.update()

    def zoom(self, event=None, x=500, y=500):
        if self.state == 'unzoomed':  # zoom
            self.root.geometry(
                "{}x{}+0+0".format(int(self.work_area[2]), int(self.work_area[3])))
            self.zoomBtn.config(command=lambda: self.zoom(x=0, y=0))
            self.state = 'zoomed'
            self.grip1.place_forget()
            self.grip2.place_forget()
            self.grip3.place_forget()
            self.grip4.place_forget()
        elif self.state == 'zoomed':  # unzoom
            self.root.geometry(
                '{}x{}+{}+{}'.format(int(self.i_width), int(self.i_height), int(x), int(y)))
            self.zoomBtn.config(command=lambda: self.zoom())
            self.state = 'unzoomed'
            self.grip1.place(relx=1.0, rely=1.0, anchor="se")
            self.grip2.place(relx=0.0, rely=1.0, anchor="sw")
            self.grip3.place(relx=1.0, rely=0.0, anchor="ne")
            self.grip4.place(relx=0.0, rely=0.0, anchor="nw")
        self.update()

    def StartMove(self, event, title=False):
        self.flags, self.hcursor, (self.cx, self.cy) = win32gui.GetCursorInfo()
        self._offsetx = event.x
        if title == True:
            self._offsetx = event.x
        self._offsety = event.y
        if self.state == "zoomed":
            self._old_offsetx = self._offsetx
            self._offsetx = self._offsetx / self.root.winfo_width() * self.i_width

    def OnMotion(self, event):
        x1 = self.root.winfo_pointerx()
        y1 = self.root.winfo_pointery()
        self.x = x1 - self._offsetx
        self.y = y1 - self._offsety
        if self.state == "zoomed":
            self.x = x1 - self._old_offsetx + self._offsetx
            self.zoom(x=self.x, y=self.y)
        else:
            self.root.geometry('+{x}+{y}'.format(x=int(self.x), y=int(self.y)))
        self.update()

root = tk.Tk()
win = Win(root)
root.mainloop()