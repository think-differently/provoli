import tkinter as tk

OPTIONS = [
"Jan",
"Feb",
"Mar"
] #etc

root = tk.Tk()
class App:
    def __init__(self):
        variable = tk.StringVar(root)
        variable.set(OPTIONS[0]) # default value
        w = tk.OptionMenu(root, variable, *OPTIONS)
        w.pack()
        b = tk.Button(text="Iconify", command=self.mini)
        b.pack()
    def mini(self, event=None):
        root.iconify()
        # root.after(1000, root.deiconify)
    

a = App()
root.mainloop()