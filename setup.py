import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="provoli-ChristianSirolli",
    version="1.0.0.0",
    author="Christian Sirolli",
    author_email="christian.sirolli@gmail.com",
    description="Provoli is a modern wrapper for Tkinter.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/think-differently/provoli",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)