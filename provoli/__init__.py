'''
    Provoli allows for more custom application decoration than is
    easily possible with normal tkinter. To use, create a tk instance
    and then call BeatifulTk.App() with the following parameters:

     * root: tkinter instance
     * title: Title of application, displayed at center of
        title bar (default='BeatifulTk Window')
     * icon: Application icon used for taskbar and title bar
        icon (default='desktop-solid.ico', credits: font-awesome)
     * min_icon: Minimize button image used in title bar for
        minimize button, which minimizes window; typically some
        sort of horizontal line (default='minimize.gif', credits:
        Christian Sirolli)
     * zoom_icon: Zoomed button image used in title bar for zoom
        button when application is in a 'zoomed' state (default=
        'zoom.gif', credits: Christian Sirolli)
     * zoom_alt_icon: Unzoomed button image used in title bar for
        zoom button when application is in a 'unzoomed' state
        (default='zoom.gif', credits: Christian Sirolli)
     * close_icon: Close button image used in title bar for close
        button, which exits the application (default: 'close.gif',
        credits: Christian Sirolli)
     * bg: Background color of application (default: 'white')
     * titlebg: Background color of title bar (default: 'lightgray')
     * titlefg: Text color of title in title bar (default: 'black')
     * i_height: Initial unzoomed height of application (default:
        500)
     * i_width: Initial unzoomed width of application (default:
        500)
     * state: Initial application state, options: 'zoomed',
        'unzoomed' (default: 'zoomed')

    To add widgets, set their master to provoli.App(root).content
    Example usage:
        import tkinter as tk
        import provoli
        root = tk.Tk()
        my_app = provoli.App(root, "AppWindow Test").content
        label1 = tk.Label(my_app, text="Hello World!",
            bg='yellow', fg='black', anchor=tk.CENTER, width=75, height=5)
        label1.pack()
        root.mainloop()

    Features:
     * Zoom, unzoom, maximize, minimize buttons
     * Close button
     * Title in title bar
     * Icon in title bar
     * Customizable colors for application decoration
     * Dragging window
     * Drag window to top, left, right and snapping in place
        - top: zoomed
        - left: snaps to left
        - right: snaps to right
     * Defs for getting application width, height (without title bar)
     * Def for updating title bar positions and running mainloop
        - self.update as tkinter.Tk().update
        - self.mainloop as tkinter.Tk().mainloop
     * Keyboard bindings for window snapping

    Variables:
     * root
     * r_width
     * r_height
     * monitor_info
     * work_area
     * i_height
     * i_width
     * titlebg
     * titlefg
     * title
     * bg
     * top_height
     * _offsetx
     * _offsety
     * x
     * y
     * __zoom__
     * __minimize__
     * __close__
     * state
     * root_frame
     * top
     * content
     * icon
     * __icon__
     * close_icon
     * closeBtn
     * zoom_icon
     * zoom_alt_icon
     * zoom_img
     * zoom_alt_img
     * zoomBtn
     * min_icon
     * minimize_img
     * minimizeBtn
     * title_bar
     * res
     * _old_offsetx
     * flags
     * hcursor

    Definitions:
     * __init__
     * set_appwindow
     * getAccentColor
     * set_bindings
     * zoom
     * minimized
     * destroy
     * geometry
     * snap_left
     * snap_right
     * winfo_width
     * winfo_height
     * update
     * StartMove
     * StopMove
     * OnMotion

    Future Features and Bug Fixes:
     * Resizing window with cursor from state='unzoomed'
'''
# pip install Pillow pypiwin32 win32gui
import os, sys
from PIL import Image, ImageTk  # pip install Pillow
import tkinter as tk
if __name__ != '__main__':
    __name__ = 'provoli'
if sys.platform == 'win32':
    nt = True
    mac = False
    linux = False
    import win32api  # pip install pypiwin32
    import win32gui  # pip install win32gui
    from ctypes import windll
    import win32con
    import winreg
    GWL_EXSTYLE = -20
    WS_EX_APPWINDOW = 0x00040000
    WS_EX_TOOLWINDOW = 0x00000080
    path = os.path.dirname(os.path.realpath(__file__))
if sys.platform == 'linux':
    nt = False
    mac = False
    linux = True
    import re
    import ctypes
    import subprocess
    from xpybutil import event # pip install xpybutil
    out = subprocess.check_output(['xprop','-root','_NET_WORKAREA'])
    workarea_tokens = re.split('=|,',out.decode())
    path = os.path.dirname(os.path.realpath(__file__))
if sys.platform == 'darwin':
    nt = False
    mac = True
    linux = False
    path = '@'+os.path.dirname(os.path.realpath(__file__))
_support_default_root = 1
_default_root = None


class App():
    def __init__(self, root, title='', icon=path+'\\img\\icon.bmp', min_icon=path+'\\img\\minimize.gif', zoom_icon=path+'\\img\\zoom.gif', zoom_alt_icon=path+'\\img\\zoom_alt.gif', close_icon=path+'\\img\\close.gif', bg='white', titlebg='lightgray', titlefg='black', i_height=500, i_width=500, state='zoomed', top_height=30, zoom=True, minimize=True, close=True, resize=True):
        self.root = root
        self.r_width = self.root.winfo_width()
        self.r_height = self.root.winfo_height()
        self.root.wm_title(title)
        if nt == True:
            self.monitor_info = win32api.GetMonitorInfo(
                win32api.MonitorFromPoint((0, 0)))
            self.work_area = self.monitor_info.get("Work")
        elif linux == True:
            self.work_area = workarea_tokens[1:5]
        else:
            self.work_area = (0,0,0,0)
        
        self.geometry("{}x{}+0+0".format(int(self.work_area[2]), int(self.work_area[3])))
        self.r_width = self.root.winfo_width()
        self.r_height = self.root.winfo_height()
        self.i_height = i_height
        self.i_width = i_width
        self.titlebg = titlebg
        self.titlefg = titlefg
        self.title = title
        self.bg = bg
        self.top_height = top_height
        self._offsetx = 0
        self._offsety = 0
        self.x = 0
        self.y = 0
        self.__zoom__ = zoom
        self.__minimize__ = minimize
        self.__close__ = close
        self.__resize__ = resize

        self.root.overrideredirect(True)
        self.root.after(1, self.set_appwindow)
        self.root.lift()
        if nt:
            self.root.iconbitmap(icon)
        elif linux:
            iconimg = ImageTk.PhotoImage(file=icon)
            self.root.call('wm', 'iconphoto', self.root._w, iconimg)

        self.state = state
        if nt or mac:
            self.root.state(self.state)  # Windows and macOS zoomed
        elif linux and self.state == "zoomed":
            self.root.attributes('-fullscreen', True)
        self.root.update_idletasks()
        self.root.update()

        self.r_width = self.root.winfo_width()
        self.r_height = self.root.winfo_height()

        self.root_frame = tk.Frame(
            self.root, height=self.work_area[3], width=self.winfo_width(), bg=self.bg, highlightthickness=0, highlightbackground=self.getAccentColor())
        self.root_frame.pack(fill=tk.BOTH, expand=1)
        self.root_frame.grid_columnconfigure(0, weight=2, uniform="column")

        self.top = tk.Frame(self.root_frame, width=self.winfo_width(), height=self.top_height, bg=self.titlebg)
        self.top.grid(row=0, column=0)

        self.content = tk.Frame(self.root_frame, bg=self.bg, width=self.winfo_width(), height=self.winfo_height())
        self.content.grid(row=1, column=0)

        self.icon = icon
        self.icon_img = Image.open(self.icon).resize((20, 20), Image.ANTIALIAS)
        self.__icon__ = tk.Label(self.top, bg=self.titlebg)
        self.__icon__.image = ImageTk.PhotoImage(image=self.icon_img)
        self.__icon__.config(image=self.__icon__.image)
        self.__icon__.pack(side=tk.LEFT, anchor=tk.N, padx=5, pady=5)

        self.close_icon = close_icon
        self.close_img = Image.open(self.close_icon).resize((20, 20), Image.ANTIALIAS)
        self.closeBtn = tk.Button(self.top, command=lambda: self.quit(), width=25, height=25, padx=5, highlightthickness=0, bd=0, activebackground="red", bg=self.titlebg, disabledforeground="darkgray")
        self.closeBtn.image = ImageTk.PhotoImage(image=self.close_img)
        self.closeBtn.config(image=self.closeBtn.image)
        self.closeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)

        self.zoom_icon = zoom_icon
        self.zoom_alt_icon = zoom_alt_icon
        self.zoom_img = Image.open(self.zoom_icon).resize((20, 20), Image.ANTIALIAS)
        self.zoom_alt_img = Image.open(self.zoom_alt_icon).resize((20, 20), Image.ANTIALIAS)
        self.zoomBtn = tk.Button(self.top, command=lambda: self.zoom(x=self.x, y=self.x), width=25, height=25, padx=5, highlightthickness=0, bd=0, activebackground="gray", bg=self.titlebg, disabledforeground="darkgray")
        self.zoomBtn.image = ImageTk.PhotoImage(image=self.zoom_img)
        self.zoomBtn.alt_image = ImageTk.PhotoImage(image=self.zoom_alt_img)
        self.zoomBtn.config(image=self.zoomBtn.image)
        self.zoomBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)

        self.min_icon = min_icon
        self.minimize_img = Image.open(self.min_icon).resize((20, 20), Image.ANTIALIAS)
        self.minimizeBtn = tk.Button(self.top, command=self.minimized, width=25, height=25, padx=5, highlightthickness=0, bd=0, activebackground="gray", bg=self.titlebg, disabledforeground="darkgray")
        self.minimizeBtn.image = ImageTk.PhotoImage(image=self.minimize_img)
        self.minimizeBtn.config(image=self.minimizeBtn.image)
        self.minimizeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)

        self.title_bar = tk.Label(self.top, bg=self.titlebg, fg=self.titlefg, text=self.title, anchor=tk.N, width=self.winfo_width(), height=1)
        self.title_bar.pack(side=tk.LEFT, fill=tk.X, anchor=tk.N, padx=5, pady=5)

        if self.__close__ == False:
            self.closeBtn.config(state=tk.DISABLED)
        if self.__zoom__ == False:
            self.zoomBtn.config(state=tk.DISABLED)
        if self.__minimize__ == False:
            self.minimizeBtn.config(state=tk.DISABLED)

        # self.menubar = Menu(self.top)
        # self.file_menu = Menu(self.menubar, tearoff=tk.FALSE)
        # self.help_menu = Menu(self.menubar, tearoff=tk.FALSE)
        # self.menubar.add_cascade(label='File', menu=self.file_menu)
        # self.menubar.add_cascade(label='Help', menu=self.help_menu)
        # self.file_menu.add_command(label='Exit', command=self.root.destroy)

        self.set_bindings()
        self.update()

    def bind(self, binding, bindTo):
        self.root.bind(binding, bindTo)
        
    def update(self):
        try:
            self.root.update()
            self.root.update_idletasks()
            self.r_width = self.root.winfo_width()
            self.r_height = self.root.winfo_height()
            print("root width", (self.root.winfo_width(), self.root.winfo_height()))
            print("r width", (self.r_width, self.r_height))

            self.root_frame.pack(fill=tk.BOTH, expand=1)
            # self.top.pack(side=tk.TOP, fill=tk.X, expand=1, anchor=tk.N)
            self.top.grid(row=0, column=0)
            self.content.config(width=self.winfo_width(), height=self.winfo_height())
            self.content.grid(row=1, column=0)
            self.__icon__.pack(side=tk.LEFT, anchor=tk.N, padx=5)
            self.closeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
            self.zoomBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
            self.minimizeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
            self.title_bar.pack(side=tk.LEFT, fill=tk.X, anchor=tk.N, padx=5, pady=5)
            if self.state == 'unzoomed':
                self.x = self.root.winfo_rootx()
                self.y = self.root.winfo_rooty()
            self.root.update()
        except:
            self.root.destroy()

    def getAccentColor(self):
        """
        Return the Windows 10 accent color used by the user in a HEX format
        """
        try:
            # Open the registry
            registry = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)
            # Navigate to the key that contains the accent color info
            key = winreg.OpenKey(
                registry, r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Accent')
            # Read the value in a REG_DWORD format
            key_value = winreg.QueryValueEx(key, 'AccentColorMenu')
            # Convert the interger to Hex and remove its offset
            accent_int = key_value[0]
            # Remove FF offset and convert to HEX again
            accent_hex = hex(accent_int+4278190080)
            accent_hex = str(accent_hex)[4:-1]  # Remove prefix and suffix
            # The HEX value was originally in a BGR order, instead of RGB,
            # so we reverse it...
            accent = accent_hex[4:6]+accent_hex[2:4]+accent_hex[0:2]
            return '#'+accent
        except:
            return '#DCCFCE'

    def set_appwindow(self, event=None):
        if nt:
            hwnd = windll.user32.GetParent(self.root.winfo_id())
            style = windll.user32.GetWindowLongW(hwnd, GWL_EXSTYLE)
            style = style | WS_EX_APPWINDOW
            self.res = windll.user32.SetWindowLongW(hwnd, GWL_EXSTYLE, style)
            self.root.wm_withdraw()
            self.root.after(10, lambda: self.root.wm_deiconify())
            self.root.focus_set()
            self.root.focus_force()
            self.root.protocol('WM_DELETE_WINDOW', self.quit)
            self.root.update_idletasks()

    def set_bindings(self):
        self.root.bind("<Alt-F4>", self.destroy)
        self.root.bind("<Control-Alt-m>", self.minimized)
        self.root.bind("<Control-Alt-z>", self.zoom)
        if self.__resize__ == True:
            self.top.bind(
                "<ButtonPress-1>", self.StartMove)
            self.top.bind(
                "<ButtonRelease-1>", self.StopMove)
            self.top.bind(
                "<B1-Motion>", self.OnMotion)
            self.title_bar.bind(
                "<ButtonPress-1>", lambda event, title=True: self.StartMove(event, title))
            self.title_bar.bind(
                "<ButtonRelease-1>", self.StopMove)
            self.title_bar.bind(
                "<B1-Motion>", self.OnMotion)
            if linux or mac:
                self.root.bind("<Super_L><Left>", self.snap_left)
                self.root.bind("<Super_L><Right>", self.snap_right)
                self.root.bind("<Super_L><Up>", lambda event: self.zoom(
                    event) if self.state == 'zoomed' else None)
                self.root.bind("<Super_L><Down>", lambda event: self.zoom(
                    event, self.x, self.y) if self.state == 'zoomed' else self.minimized(event))
                self.root.bind("<Super_R><Left>", self.snap_left)
                self.root.bind("<Super_R><Right>", self.snap_right)
                self.root.bind("<Super_R><Up>", lambda event: self.zoom(
                    event) if self.state == 'zoomed' else None)
                self.root.bind("<Super_R><Down>", lambda event: self.zoom(
                    event, self.x, self.y) if self.state == 'zoomed' else self.minimized(event))
            elif nt:
                self.root.bind("<Win_L><Left>", self.snap_left)
                self.root.bind("<Win_L><Right>", self.snap_right)
                self.root.bind("<Win_L><Up>", lambda event: self.zoom(
                    event) if self.state == 'zoomed' else None)
                self.root.bind("<Win_L><Down>", lambda event: self.zoom(
                    event, self.x, self.y) if self.state == 'zoomed' else self.minimized(event))
                self.root.bind("<Win_R><Left>", self.snap_left)
                self.root.bind("<Win_R><Right>", self.snap_right)
                self.root.bind("<Win_R><Up>", lambda event: self.zoom(event)
                            if self.state == 'zoomed' else None)
                self.root.bind("<Win_R><Down>", lambda event: self.zoom(
                    event, self.x, self.y) if self.state == 'zoomed' else self.minimized(event))

    def zoom(self, event=None, x=None, y=None):
        if self.state == 'unzoomed':  # zoom
            self.root.geometry(
                "{}x{}+0+0".format(int(self.work_area[2]), int(self.work_area[3])))
            self.root_frame.config(highlightthickness=0)
            self.update()
            self.top.grid(row=0, column=0)
            self.zoomBtn.config(command=lambda: self.zoom(
                x=0, y=0), image=self.zoomBtn.image)
            self.state = 'zoomed'
        elif self.state == 'zoomed':  # unzoom
            if x == None:
                x = self.x
            if y == None:
                y = self.y
            self.root.geometry(
                '{}x{}+{}+{}'.format(int(self.i_width), int(self.i_height), int(x), int(y)))
            self.root_frame.config(highlightthickness=2)
            self.update()
            self.top.grid(row=0, column=0)
            self.zoomBtn.config(command=lambda: self.zoom(),
                                image=self.zoomBtn.alt_image)
            self.state = 'unzoomed'

    def minimized(self, event=None):
        if nt:
            Minimize = win32gui.GetForegroundWindow()
            win32gui.ShowWindow(Minimize, win32con.SW_MINIMIZE)
        if linux:
            # event
            print('Cannot minimize')

    def destroy(self, event=None):
        self.root.destroy()

    def quit(self, event=None):
        self.root.quit()
        self.root.destroy()

    def geometry(self, size):
        self.root.geometry(size)

    def snap_left(self, event=None):
        # splits to the left if window is dragged to the left side of screen
        self.geometry(
            "{}x{}+0+0".format(int(self.work_area[2] / 2), int(self.work_area[3])))
        self.update()

    def snap_right(self, event=None):
        # splits to the right if window is dragged to the right side of screen
        self.root.geometry("{}x{}+{}+0".format(int(self.work_area[2] / 2), int(
            self.work_area[3]), int(self.work_area[2] / 2)))
        self.update()

    def winfo_width(self):
        return self.r_width

    def winfo_height(self):
        return self.r_height-self.top_height
    
    def mainloop(self):
        self.root.mainloop()
    
    def StartMove(self, event, title=False):
        if nt:
            self.flags, self.hcursor, (self.cx, self.cy) = win32gui.GetCursorInfo()
        if linux:
            Xlib = ctypes.CDLL("libX11.so.6")
            display = Xlib.XOpenDisplay(None)
            if display == 0: sys.exit(2)
            w = Xlib.XRootWindow(display, ctypes.c_int(0))
            (root_id, child_id) = (ctypes.c_uint32(), ctypes.c_uint32())
            (root_x, root_y, win_x, win_y) = (ctypes.c_int(), ctypes.c_int(), ctypes.c_int(), ctypes.c_int())
            mask = ctypes.c_uint()
            ret = Xlib.XQueryPointer(display, ctypes.c_uint32(w), ctypes.byref(root_id), ctypes.byref(child_id),
                                    ctypes.byref(root_x), ctypes.byref(root_y),
                                    ctypes.byref(win_x), ctypes.byref(win_y), ctypes.byref(mask))
            if ret == 0: sys.exit(1)
            self.cx, self.cy = root_x.value, root_y.value
        self._offsetx = event.x
        if title == True:
            self._offsetx = event.x + 30
        self._offsety = event.y
        if self.state == "zoomed":
            self._old_offsetx = self._offsetx
            self._offsetx = self._offsetx / self.root.winfo_width() * self.i_width

    def StopMove(self, event):
        if nt:
            self.flags, self.hcursor, (self.cx, self.cy) = win32gui.GetCursorInfo()
        if linux:
            Xlib = ctypes.CDLL("libX11.so.6")
            display = Xlib.XOpenDisplay(None)
            if display == 0: sys.exit(2)
            w = Xlib.XRootWindow(display, ctypes.c_int(0))
            (root_id, child_id) = (ctypes.c_uint32(), ctypes.c_uint32())
            (root_x, root_y, win_x, win_y) = (ctypes.c_int(), ctypes.c_int(), ctypes.c_int(), ctypes.c_int())
            mask = ctypes.c_uint()
            ret = Xlib.XQueryPointer(display, ctypes.c_uint32(w), ctypes.byref(root_id), ctypes.byref(child_id),
                                    ctypes.byref(root_x), ctypes.byref(root_y),
                                    ctypes.byref(win_x), ctypes.byref(win_y), ctypes.byref(mask))
            if ret == 0: sys.exit(1)
            self.cx, self.cy = root_x.value, root_y.value
        if self.cy <= 0:
            self.zoom()
        elif self.cx <= 0:
            self.snap_left()
        elif self.cx >= int(self.work_area[2]) - 1:
            self.snap_right()
        self._offsetx = 0
        self._offsety = 0
        self.x = self.root.winfo_rootx()
        self.y = self.root.winfo_rooty()

    def OnMotion(self, event):
        x1 = self.root.winfo_pointerx()
        y1 = self.root.winfo_pointery()
        self.x = x1 - self._offsetx
        self.y = y1 - self._offsety
        if self.state == "zoomed":
            self.x = x1 - self._old_offsetx + self._offsetx
            self.zoom(x=self.x, y=self.y)
        else:
            self.root.geometry('+{x}+{y}'.format(x=int(round(self.x, 0)), y=int(round(self.y, 0))))
        self.update()


if __name__ == '__main__':
    root = tk.Tk()
    if nt:
        icon = path+'/img/icon.ico'
    elif linux:
        icon = path+'/img/icon.gif'
    my_app = App(root, "Beautiful Application", icon=icon, min_icon=path+'/img/minimize.gif', zoom_icon=path+'/img/zoom.gif', zoom_alt_icon=path+'/img/zoom_alt.gif', close_icon=path+'/img/close.gif', top_height=100)
    label1 = tk.Label(my_app.content, text="Hello World!", bg='yellow', fg='black', anchor=tk.CENTER, width=75, height=5)
    label1.pack(anchor=tk.CENTER, side=tk.LEFT)
    root.mainloop()